# Task: Implement Design

The goal is to have a static implementation of the given screenshots. The website should be responsive with three viewports.

High quality of the work is more important than the implementation speed.

Please use placeholders as images, no need to screenshot the designs.