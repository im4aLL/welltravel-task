var gulp         = require('gulp');
var browserSync  = require('browser-sync').create();
var reload       = browserSync.reload;
var sass         = require('gulp-sass');
var source       = require('vinyl-source-stream');
var rename       = require('gulp-rename');
var cssnano      = require('gulp-cssnano');
var watch        = require('gulp-watch');
var autoprefixer = require('gulp-autoprefixer');

var config = {
    sass: {
        source: './assets_source/sass/style.scss',
        dist: './assets/css',
        fileName: 'style.css',
        minifiedFileName: 'style.min.css',
        watch: './assets_source/sass/**/*.scss'
    },
    sync: {
        server: true
    }
};
// https://www.browsersync.io/docs/options/


// sass to css
gulp.task('sass', function () {
    gulp.src(config.sass.source)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(config.sass.dist))
        .pipe(browserSync.stream());
});

// default task adn watch
gulp.task('serve', ['sass'], function() {
    browserSync.init(config.sync);

    watch(config.sass.watch, function(){
        gulp.start('sass');
    });

    watch('./*.html', function(){
        reload();
    });
});

// default task
gulp.task('default', ['serve']);

// gulp build and minify things
gulp.task('build', ['sass'], function(){
    gulp.src(config.sass.dist + '/' + config.sass.fileName)
        .pipe(cssnano())
        .pipe(rename(config.sass.minifiedFileName))
        .pipe(gulp.dest(config.sass.dist));
});
